# Tender Mint

## Start Server

./tendermint init

./tendermint

## Client

./dummy -addr tcp://0.0.0.0:46659
http://localhost:46657/status

[https://github.com/tendermint/abci](https://github.com/tendermint/abci)