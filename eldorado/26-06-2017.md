#  [Diário de bordo] 26 de Junho de 2017

[Tarefa  2586](http://home/areas/SisCorp/suporte/_layouts/15/listform.aspx?PageType=6&ListId=%7B4FD8FE6A%2DDE85%2D4544%2DA5F2%2D3A72676A6DB5%7D&ID=2586)
[Tarefa  2567](http://home/areas/SisCorp/suporte/_layouts/15/listform.aspx?PageType=6&ListId=%7B4FD8FE6A%2DDE85%2D4544%2DA5F2%2D3A72676A6DB5%7D&ID=2567)

[Tarefa  2574](http://home/areas/SisCorp/suporte/_layouts/15/listform.aspx?PageType=6&ListId=%7B4FD8FE6A%2DDE85%2D4544%2DA5F2%2D3A72676A6DB5%7D&ID=2574)

Os lançamentos de viagens não possuem as informações iguais impossiblitando a consolidação dos mesmos, por ser um problema causado pela alteração manual de todos lançamentos a serem agrupados sugeri a criação de uma funcionalidade semelhante a de rateio. Na tela seria apresentada duas colunas uma com o radio buttom indicando de qual lançamentos seria originado as informações e outra coluna com um check botton indicando para quais lançamentos receberiam as informações.


[Tarefa  2604](http://home/areas/SisCorp/suporte/_layouts/15/listform.aspx?PageType=6&ListId=%7B4FD8FE6A%2DDE85%2D4544%2DA5F2%2D3A72676A6DB5%7D&ID=2604)

Segundo especificaçao do Paulo Carrara e a [referencia](http://stefanteixeira.com.br/2015/03/06/pipelines-complexos-no-jenkins-com-buildflow-plugin/) sugerida por ele, criei o os job de SMWEB_PipeLine, SMWEB_Integracao e SMWEB_Homologacao utlizando o [Build Pipeline Plugin](https://wiki.jenkins-ci.org/display/JENKINS/Build+Pipeline+Plugin) para visulizar a execução dos jobs, e sem utilzar o arquivo .jenkisfile para declaração das etapas de execução e a utlização das tarefas do msbuild. A ideia inicial foi seguiria o seguindo fluxo:

- SMWEB_PipeLine => Build Aplicação  => Execução dos teste unitarios (quando existirem) => Notificações
- SMWEB_Integracao => SMWEB_PipeLine => Publicação no ambiente de integração
- SMWEB_Homologacao => SMWEB_PipeLine => Publicação no ambiente de homologação

O jenkins(serv249) esta configurado para executar apenas um job por vez o que impossiblitou a implementação do processo sugerido, na forma que tenho conhecimento, sendo necessário a alteração do numero de executores para no minimo 3. Após essa alteração foi possivel fazer a criação dos jobs. Durante a criação do job de Integração  pensei: vou ter que criar manualmente todos esses passo para um novo pipeline de projeto? que trabalho de corno.

Pensando em uma maneira de simplificar a criação de novos pipelines criei os jobs de nuget_restore e msbuild que seriam responsaveis por restauração das dependeicas do projeto e pelo build e publicação (quando necessario) da aplicação, recebendo por parametro as configurações para execução, para isso utlizei o plugin [Parameterized Trigger](https://wiki.jenkins-ci.org/display/JENKINS/Parameterized+Trigger+Plugin), a ideia foi legal deu certo a criação dos jobs genericos de restore e build, no entanto parace que o serv249 não aguentou a utlização dos 3 nos de execução e travou por falta de memória, não possibilitando a homologação do Paulo.

Antes de ter feito essas alterações deveria ter comentado com o Paulo mais acabei fazendo sem falar com ele, de novo, tenho que melhorar esse tipo de atitude.  

Apos o Kauan reiniciar o serviço do jenkins ele voltou e fica faltando apenas a validação do Paulo sobre o criação dos jobs.



[Tarefa  2610](http://home/areas/SisCorp/suporte/_layouts/15/listform.aspx?PageType=6&ListId=%7B4FD8FE6A%2DDE85%2D4544%2DA5F2%2D3A72676A6DB5%7D&ID=2610)

Iniciando o processo de analise da tarefa 2610, ao tentar editar uma das provisões informadas na tarefa no mes de junho/2017 não consegui localizar essas provisões na base do Serv167\\ipe_int, vou ver com o Kauan se a base provisão esta atualizada.

Vixe todos estao away em campinas (8:51), vou tentar identificar o fluxo de edição de uma provisão.

Pedi para o Kauan atualizar a base de provisões no Serv\\167\\ipe_int estou aguardando a atualização base, seria interessante que esse processo de atualização da base pudesse ser feito dentro jenkins. Restore feito no ambiente de homologação por volta das 10:30

Achei um metodo Provision.Save espero que seja ele responável por salvar a provisão, como não sei nada de provisão e bem complicado saber se estou no método certo, pela implementação dele é possível que o problema esteja no js (edicao.controller.js).

Entrei em contato com a Daine Cristina Pelucci Barbosa para ela me explicar como funciona a edição de provisão, era só preecher os campos e salvar kkkk. No ambiente de homologação e de desenvolvimento deu certo a edição. Vou fazer o teste no ambiente de produção junto com a Daiane com ela compartilhando a tela. A edição da provisão deu certo no ambiente de produção, estou esperando a confirmação que as outras edições deram certo.

Daine me avisou que todas as alterações foram salva que já ia gerar o relatorio. Fiz alguns teste no ambiente de desenvolvimento mais não consegui simular o erro, finalizei a tarefa.

Perguntar para o Paulo sobre o que fazer com a branch manutencao_2610 ja que nao ouve alteração de codigo, o que eu devo fazer com ela? Segundo o como não teve alteração de codigo eu posso excluir a branch.

[Tarefa  2580](http://home/areas/SisCorp/suporte/_layouts/15/listform.aspx?PageType=6&ListId=%7B4FD8FE6A%2DDE85%2D4544%2DA5F2%2D3A72676A6DB5%7D&ID=2580)

Faço nem ideia do fazer nessa tarefa, ajustar o sistema de provisões para considerar a baixa das duas férias (inclusive do abono quando aplicável) e nos retornar em qual mês já estará disponíve (??).

Todos estam no almoço vou continuar lendo o codigo para ver se tenho alguma ideia de como realizar essa tarefa.

Após ler a descrição da tarefa percebi que o pessoal do DCF ja identificação que não existe programação de duas ferias no mesmo mês, vou ver com o Paulo se ele sabe onde eu recupero esse cadastro de ferias.

Conversei com o Adriano para saber ver se ele tinha alguma ideia sobrea tarefa.

Parei essa tarefa para ver o build do jenkins junto com o Kauan, parece que ele perdeu as credenciais após ser reiniciado a força =( .

[Tarefa  2612](http://home/areas/SisCorp/suporte/_layouts/15/listform.aspx?PageType=6&ListId=%7B4FD8FE6A%2DDE85%2D4544%2DA5F2%2D3A72676A6DB5%7D&ID=2612)

Daiane me passou como exportar o relatorio de provisão de junho

- Exportar => Tipo de exportacao (Exportação de provisao) => Referencia (Junho 2017)

Segundo a Daiane apenas o relatorio da Ana Cristina Livino Cabral Elzogho que não esta pegando as alterações feita, pedi para o Adriano realizar um restore da base de produção novamente no serv167//ipe_int.

O serv167//ipe_int não possui linked sever com SERV149\\SISCORP ("Could not find server 'SERV149\\SISCORP' in sys.servers. Verify that the correct server name was specified. If necessary, execute the stored procedure sp_addlinkedserver to add the server to sys.servers.\r\nCould not use view or function 'ProvisaoDRH' because of binding errors."), para solucinar esse problema e necessario alterar a consulta que utliza esse linked server para usar não usar-lo e sim consultar na base do ambiente local, tenho que investigar onde fazer isso. ProvisaoDRH e uma view mais ao tentar acesssar a base Provissão no ipe_int estava sem permissão. Vou precisar alterar a view ProvisaoDRH para utilizar a base colaboradoresADP do ipe_int. Sem premissão para a base de colaboradoresADP. A view ProvisaoDRH utiliza a view COLABORADORESADP.ADP.DEPARTAMENTO da base colaboradoresADP que é um linked server com o AX, ja quebramos a arquitetura onde o ipe_int nao deveria possuir linked server.

O usuario da aplicação nao tem acesso ao banco colaboradoresADP no ipe_int, vou alterar a string de conexao para utlizar o windows authentication.

Toda vez que pesquiso a provisao da anao para edição me e apresentado valores diferentes.

Ao gerar as colunas do grid de edição de provisão as colunas do item Férias elas veem desordenadas, ficam variando, descobrimos que esse era o real problema do sistemas como as colunas e os campos ficam variando dava a impressão que o sistema estava errado, ainda bem que o adriano foi conversar com a Daiane. O problema foi resolvido realizando a ordenação da lista de calculos pelo tipo.
