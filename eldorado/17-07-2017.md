# [Diário de bordo] 17 de Julho de 2017

## Metas

- [ ] Montar ambiente local para o EldBook [Tarefa 1735](http://home/areas/SisCorp/suporte/_layouts/15/listform.aspx?PageType=6&ListId=%7B4FD8FE6A%2DDE85%2D4544%2DA5F2%2D3A72676A6DB5%7D&ID=1735)
- [ ] Publicar serviço de colaboradores no ambiente de integração [Tarefa 1735](http://home/areas/SisCorp/suporte/_layouts/15/listform.aspx?PageType=6&ListId=%7B4FD8FE6A%2DDE85%2D4544%2DA5F2%2D3A72676A6DB5%7D&ID=1735)
- [x] Recuperar foto do colaborador do AD [Tarefa 1735](http://home/areas/SisCorp/suporte/_layouts/15/listform.aspx?PageType=6&ListId=%7B4FD8FE6A%2DDE85%2D4544%2DA5F2%2D3A72676A6DB5%7D&ID=1735)

## Notas

- Segunda-feira :D, na sexta consegui montar o serviço de colaboradores com o Asp.net Core tenho alguns ajustes para fazer nele, algumas melhorias de isolamento de camadas, a idéia é isolar a camada web como infraestrutura e usando docker fazer a publicação dela como dependencia.
- Vou conversar com o Cristian para ele montar um design novo para o EldTV que ai migramos ele para HTML 5  e saimos do flash.
- [Tarefa 1735](http://home/areas/SisCorp/suporte/_layouts/15/listform.aspx?PageType=6&ListId=%7B4FD8FE6A%2DDE85%2D4544%2DA5F2%2D3A72676A6DB5%7D&ID=1735)
 - Publicação do serviço de aniversariantes no ambiente de integração.
  - A publicação e simples e como o dotnet core e autocontido basta rodar o comando dotnet [arquivo].ddl
  - Estou esperando o Kauan chegar para ver a instalação do Core no serv190
  - Estou vendo como o Prestação recupera a imagem do colaborador, ele rececupera as informações do LDAP.
  - Tentando importar o Siscorp.Framework no projeto da API Core, mais como são de Frameworks diferentes esta dando erro. Pode ser que não funcione no linux =(
  - achei a configuração para usar o Eldorado.Siscorp.Authorization basta usar a configuração no csproj  <PackageTargetFallback>$(PackageTargetFallback);net461</PackageTargetFallback>
  - asp.net core não portou as referencia para o Active Directory (AD), que usamos na parte de autenticação e para recuperar a foto, vou conversar com o Tiago Silva para ver se o sharepoint não poderia recuperar a imagem do AD.
  - conversando com o Kauan sobre acessar o LDAP ele me passou que a base de ColaboradoresADP tem uma view que acessa o LDAP que eu podia tentar recuperar a foto por lá, com essa alteração não precisei referenciar o Sicorp.Framework.Authorization. Agora o problema e o tamanho da foto do AD que e 64x64. Vou tentar reunir com o Adriano para comentar esses problema e sugerir de criar uma página no sharepoint para recuperar a foto ao invês de uitlizar uma consulta SQL.
  - Kauan fez a instalação do dotnet core no serv190 quando fui rodar a aplicação o server me retornou que não tinha a SDK instalada, amanhã vou ver com o Efrain se ele tem idéia do que pode ser esse problema.
  - estou começando a gostar do Dotnet Core.
  - para alterar a foto utlizamos o Exchange é nele ficam salvas as fotos, para recuperar a foto em um tamanho expecifico temos que utlizar o serviço https://webmailv2.eldorado.org.br/ews/exchange.asmx/s/GetUserPhoto?email=zama.braga@eldorado.org.br&size=HR240x240, https://msdn.microsoft.com/pt-br/library/office/jj190905(v=exchg.150).aspx
