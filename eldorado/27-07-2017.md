# [Diário de bordo] 27 de Julho de 2017

## Metas

- [x] Alterar o processamento do timesheet para salvar a alocação [tarefa 2508](http://home/areas/SisCorp/suporte/_layouts/15/listform.aspx?PageType=6&ListId=%7B4FD8FE6A%2DDE85%2D4544%2DA5F2%2D3A72676A6DB5%7D&ID=2508)
 - [x] Serviços para calculo de conta beneficio
 - [x] Serviços para calculo de conta encargo
- [x] Alterar exportar timesheet para utilizar o resultado do calculo da alocação [tarefa 2508](http://home/areas/SisCorp/suporte/_layouts/15/listform.aspx?PageType=6&ListId=%7B4FD8FE6A%2DDE85%2D4544%2DA5F2%2D3A72676A6DB5%7D&ID=2508)
- [x] Alterar exportar al para utlizar o resutlardo do calculo da alocação [tarefa 2508](http://home/areas/SisCorp/suporte/_layouts/15/listform.aspx?PageType=6&ListId=%7B4FD8FE6A%2DDE85%2D4544%2DA5F2%2D3A72676A6DB5%7D&ID=2508)
- [x] Alterar exportar timesheet de transferência para utilizar o resultado do calculo da alocação [tarefa 2508](http://home/areas/SisCorp/suporte/_layouts/15/listform.aspx?PageType=6&ListId=%7B4FD8FE6A%2DDE85%2D4544%2DA5F2%2D3A72676A6DB5%7D&ID=2508)
- [x] Alterar exportar al de transferência para utlizar o resultado do calculo da alocação [tarefa 2508](http://home/areas/SisCorp/suporte/_layouts/15/listform.aspx?PageType=6&ListId=%7B4FD8FE6A%2DDE85%2D4544%2DA5F2%2D3A72676A6DB5%7D&ID=2508)

## Notas

- Montei um e-mail sobre as minhas dúvidas em relação ao nosso padrão
- [Tarefa 2508](http://home/areas/SisCorp/suporte/_layouts/15/listform.aspx?PageType=6&ListId=%7B4FD8FE6A%2DDE85%2D4544%2DA5F2%2D3A72676A6DB5%7D&ID=2508)
 - Alteração dos serviços de calculo de beneficios:
   - CalculoBeneficioAlimentacaoService
   - CalculoBeneficioAssistenciaMedicaService
   - CalculoBeneficioAssistenciaOdontologicaService
   - CalculoBeneficioAuxilioCrecheService
   - CalculoBeneficioCestaNatalService
   - CalculoBeneficioEstacionamentoService
   - CalculoBeneficioIdiomasService
   - CalculoBeneficioImpAssistMedicaService
   - CalculoBeneficioLeasingService
   - CalculoBeneficioPgblMensalService
   - CalculoBeneficioPgblRetroativoService
   - CalculoBeneficioSeguroVidaService
   - CalculoBeneficioService
   - CalculoBeneficioTaxaAdmCIEEService
   - CalculoBeneficioTransporteService
   - CalculoBeneficioServiceTestFixture
 - Alteração dos serviços de calculos de encargos:
   - CalculoEncargoFgtsAcrescimoSobreSalarioService
   - CalculoEncargoFgtsSobreAvisoPrevioService
   - CalculoEncargoFgtsSobreDecimoTerceiroIndenizadoService
   - CalculoEncargoFgtsSobreDecimoTerceiroService
   - CalculoEncargoFgtsSobreFeriasIndenizadasService
   - CalculoEncargoFgtsSobreFeriasService
   - CalculoEncargoFgtsSobreSalarioService
   - CalculoEncargoInssSobreAvisoPrevioService
   - CalculoEncargoInssSobreDecimoTerceiroIndenizadoService
   - CalculoEncargoInssSobreDecimoTerceiroService
   - CalculoEncargoInssSobreFeriasIndenizadasService
   - CalculoEncargoInssSobreFeriasService
   - CalculoEncargoInssSobreSalarioService
   - CalculoEncargoPisSobreAvisoPrevioService
   - CalculoEncargoPisSobreDecimoTerceiroIndenizadoService
   - CalculoEncargoPisSobreDecimoTerceiroService
   - CalculoEncargoPisSobreFeriasIndenizadasService
   - CalculoEncargoPisSobreFeriasService
   - CalculoEncargoPisSobreSalarioService
   - CalculoEncargoProvisaoAvisoPrevioService
   - CalculoEncargoProvisaoDecimoTerceiroIndenizadoService
   - CalculoEncargoProvisaoDecimoTerceiroService
   - CalculoEncargoProvisaoFeriasEstagiarioService
   - CalculoEncargoProvisaoFeriasIndenizadasService
   - CalculoEncargoProvisaoFeriasService
   - CalculoEncargoProvisaoMultaFgtsService
   - CalculoEncargoService
   - CalculoEncargoServiceTestFixture
 - A parte de estorno do serviço esta um pouco mais chata tive que criar um metodo nos repositorios ContaBeneficioRepository, ContaCustoInfraestruturaRepository, ContaEncargoEldoradoRepository, ContaEncargoRepository e ContaSalarioRepository para retornar o resultado por alocação.
 - Tive que alterar o serviço de estorno para receber as alcoações referentes ao estorno
 - Estou tentando corrigir o teste Deveria_Calcular_Custos_Para_Transferencia_Na_Mesma_Unidade_Entre_Projetos_De_Categoria_Um_E_Cinco
 ele não esta fazendo sentido o valor que ele esta apresentando =(. Consegui entender o problema era o nomes dos metodos que não estavam fazendo sentidos eu troquei o nome do EstornoService para ProjetosAdministrativosService e os metodos dele para GerarCredito e GerarDebito ficou bem mais claro.
 - Testes :
   - TimeSheet de Transferencia Projeto 1 para 5 - passou
   - TimeSheet de Transferencia Projeto 5 para 1 - passou
   - AL de Transferencia Projeto 1 para 5 - passou
   - AL de Transferencia Projeto 5 para 1 - passou
 - vou disponibilizar para teste
 - o timesheet_pipeline esta quebrado, estou vendo o log para identificar o que esta errado, no ambiente local os testes passaram.
 - pedi para o Paulo rodar na maquina dele os teste para ver se e alguma configuração que eu fiz. Estou achando que o problema e linguagem do servidor.
 - forcei o idioma nos teste unitários para ver se é o idioma do servidor esta cofigurado corretamente. Ele não esta configurado o padrão portugues do Brasil.
- Hellen entrou em contato dizendo que a rubrica de viagens esta com diferença no total do projeto hackatruck, e que esta com urgencia [tarefa 2716](http://home/areas/SisCorp/suporte/_layouts/15/listform.aspx?PageType=6&ListId=%7B4FD8FE6A%2DDE85%2D4544%2DA5F2%2D3A72676A6DB5%7D&ID=2716), vou chegar amanha mais cedo para tentar resolver esse problema pela manhã
